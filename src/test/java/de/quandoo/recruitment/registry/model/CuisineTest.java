package de.quandoo.recruitment.registry.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CuisineTest {

    @Test
    public void shouldSetName() {
        Cuisine c = new Cuisine("mediterranean");
        assertEquals("mediterranean", c.getName());
    }

    @Test
    public void shouldSupportEquals() {
        assertEquals(new Cuisine("c1"), new Cuisine("c1"));
        assertNotEquals(new Cuisine("c1"), new Cuisine("c2"));
    }

    @Test
    public void shouldSupportCompareTo() {
        assertEquals(-1, new Cuisine("a").compareTo(new Cuisine("b")));
        assertEquals(0, new Cuisine("a").compareTo(new Cuisine("a")));
        assertEquals(1, new Cuisine("b").compareTo(new Cuisine("a")));
    }

    @Test
    public void shouldSupportHashCode() {
        assertEquals(new Cuisine("a").hashCode(), new Cuisine("a").hashCode());
        assertNotEquals(new Cuisine("a").hashCode(), new Cuisine("b").hashCode());
        assertNotEquals(new Cuisine("cc").hashCode(), new Cuisine("").hashCode());
    }
}
