package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.Objects.requireNonNull;

public class InMemoryCuisinesRegistryFastTop implements CuisinesRegistry {
    private ConcurrentHashMap<Customer, Set<Cuisine>> customerCuisines = new ConcurrentHashMap<>();

    private HashMap<Cuisine, Set<Customer>> cuisineCustomers = new HashMap<>();
    // Keeps sorted tree for top cuisines, based on cuisineCustomers map
    private TreeSet<Cuisine> topCuisines = new TreeSet<>(
            Comparator.comparing((Cuisine c) -> cuisineCustomers.get(c).size()).reversed()
                    .thenComparing(Cuisine::getName)
    );
    // Will be used to lock topCuisines while writing/reading, supports multiple readers
    private ReadWriteLock topCuisinesLock = new ReentrantReadWriteLock();

    private synchronized void registerCuisine(final Customer customer, final Cuisine cuisine) {
        if (cuisineCustomers.containsKey(cuisine)) {

            topCuisinesLock.writeLock().lock();
            topCuisines.remove(cuisine);

            cuisineCustomers.get(cuisine).add(customer);

        } else {
            cuisineCustomers.put(cuisine, Collections.synchronizedSet(new HashSet<>(Arrays.asList(customer))));

            topCuisinesLock.writeLock().lock();
        }

        topCuisines.add(cuisine);
        topCuisinesLock.writeLock().unlock();
    }

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        requireNonNull(customer, "Customer can not be null");
        requireNonNull(cuisine, "Cuisine can not be null");

        customerCuisines.putIfAbsent(customer, Collections.synchronizedSet(new HashSet<>()));
        customerCuisines.get(customer).add(cuisine);

        registerCuisine(customer, cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        requireNonNull(cuisine, "Cuisine can not be null");
        return new ArrayList<>(cuisineCustomers.getOrDefault(cuisine, new HashSet<>()));
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        requireNonNull(customer, "Customer can not be null");
        return new ArrayList<>(customerCuisines.getOrDefault(customer, new HashSet<>()));
    }

    @Override
    public List<Cuisine> topCuisines(final int n) throws IllegalArgumentException {
        if (n < 0) {
            throw new IllegalArgumentException("Invalid argument " + n);
        }

        ArrayList<Cuisine> res = new ArrayList<>(n);

        topCuisinesLock.readLock().lock();
        Iterator<Cuisine> it = topCuisines.iterator();
        for (int i = 0; i < n; i++) {
            if (it.hasNext()) {
                res.add(it.next());
            } else {
                break;
            }
        }
        topCuisinesLock.readLock().unlock();

        return res;

    }
}
