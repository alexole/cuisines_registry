package de.quandoo.recruitment.registry.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CustomerTest {
    @Test
    public void shouldSetUuid() {
        Customer c = new Customer("peter");
        assertEquals("peter", c.getUuid());
    }

    @Test
    public void shouldSupportEquals() {
        assertEquals(new Customer("c1"), new Customer("c1"));
        assertNotEquals(new Customer("c1"), new Customer("c2"));
    }

    @Test
    public void shouldSupportCompareTo() {
        assertEquals(-1, new Customer("a").compareTo(new Customer("b")));
        assertEquals(0, new Customer("a").compareTo(new Customer("a")));
        assertEquals(1, new Customer("b").compareTo(new Customer("a")));
    }

    @Test
    public void shouldSupportHashCode() {
        assertEquals(new Customer("a").hashCode(), new Customer("a").hashCode());
        assertNotEquals(new Customer("a").hashCode(), new Customer("b").hashCode());
        assertNotEquals(new Customer("cc").hashCode(), new Customer("").hashCode());
    }
}
