package de.quandoo.recruitment.registry.model;

public class Customer implements Comparable<Customer> {

    private final String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Customer)) {
            return false;
        }

        return uuid.equals(((Customer) obj).uuid);
    }

    @Override
    public int compareTo(Customer obj) {
        return uuid.compareTo(obj.uuid);
    }

    public int hashCode() {
        int result = 13;
        result = 31 * result + (uuid == null ? 0 : uuid.hashCode());
        return result;
    }
}
