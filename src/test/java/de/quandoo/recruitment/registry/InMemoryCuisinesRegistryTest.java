package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class InMemoryCuisinesRegistryTest {

    private static final Cuisine frenchCuisine = new Cuisine("french");
    private static final Cuisine italianCuisine = new Cuisine("italian");
    private static final Cuisine spanishCuisine = new Cuisine("spanish");
    private static final Cuisine russianCuisine = new Cuisine("russian");

    private static final Customer alice = new Customer("alice");
    private static final Customer bob = new Customer("bob");
    private static final Customer peter = new Customer("peter");

    private CuisinesRegistry cuisinesRegistry;
    private final Class clazz;

    public InMemoryCuisinesRegistryTest(Class cl) {
        this.clazz = cl;
    }

    @Parameterized.Parameters(name = "{index}: Impl Class: {0}")
    public static List<Object[]> getParameters() {
        List<Object[]> params = new ArrayList<>();
        params.add(new Object[] { InMemoryCuisinesRegistryFastRegister.class });
        params.add(new Object[] { InMemoryCuisinesRegistryFastTop.class });

        return params;
    }

    @Before
    public void initObjects() throws Exception {
        cuisinesRegistry = (CuisinesRegistry) clazz.newInstance();
    }

    private <T> boolean listsMatch(List<T> l1, List<T> l2) {
        return l1.size() == l2.size() && l1.containsAll(l2) && l2.containsAll(l1);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailRegisterIfCustomerIsNull() {
        cuisinesRegistry.register(null, new Cuisine("blah"));
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailRegisterIfCuisineIsNull() {
        cuisinesRegistry.register(null, new Cuisine("blah"));
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailCustomerCuisinesIfCustomerIsNull() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailCuisineCustomersIfCuisineIsNull() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldIdentifyCustomerById() {
        cuisinesRegistry.register(new Customer("1"), frenchCuisine);
        cuisinesRegistry.register(new Customer("1"), russianCuisine);

        assertTrue(listsMatch(
                cuisinesRegistry.customerCuisines(new Customer("1")),
                Arrays.asList(russianCuisine, frenchCuisine)
        ));
    }

    @Test
    public void shouldIdentifyCuisineByName() {
        cuisinesRegistry.register(alice, new Cuisine("belarusian"));
        cuisinesRegistry.register(bob, new Cuisine("belarusian"));

        assertTrue(listsMatch(
                cuisinesRegistry.cuisineCustomers(new Cuisine("belarusian")),
                Arrays.asList(alice, bob)
        ));

        assertTrue(listsMatch(cuisinesRegistry.topCuisines(2), Arrays.asList(new Cuisine("belarusian"))));
    }

    @Test
    public void shouldReturnEmptyLists() {
        assertTrue(cuisinesRegistry.customerCuisines(new Customer("whatever")).isEmpty());
        assertTrue(cuisinesRegistry.cuisineCustomers(new Cuisine("whatsoever")).isEmpty());
        assertTrue(cuisinesRegistry.topCuisines(0).isEmpty());
        assertTrue(cuisinesRegistry.topCuisines(10).isEmpty());
    }

    @Test
    public void shouldReturnCustomerCuisines(){
        cuisinesRegistry.register(alice, russianCuisine);
        cuisinesRegistry.register(bob, italianCuisine);
        cuisinesRegistry.register(alice, spanishCuisine);

        assertTrue(listsMatch(
                cuisinesRegistry.customerCuisines(alice),
                Arrays.asList(spanishCuisine, russianCuisine)
        ));

        assertTrue(listsMatch(
                cuisinesRegistry.customerCuisines(bob),
                Arrays.asList(italianCuisine)
        ));
    }

    @Test
    public void shouldReturnCuisineCustomers(){
        cuisinesRegistry.register(alice, russianCuisine);
        cuisinesRegistry.register(bob, spanishCuisine);
        cuisinesRegistry.register(alice, spanishCuisine);
        cuisinesRegistry.register(peter, frenchCuisine);

        assertTrue(listsMatch(
                cuisinesRegistry.cuisineCustomers(spanishCuisine),
                Arrays.asList(alice, bob)
        ));
    }

    @Test
    public void shouldReturnTopCuisines() {
        cuisinesRegistry.register(new Customer("4"), italianCuisine);
        cuisinesRegistry.register(new Customer("5"), frenchCuisine);
        cuisinesRegistry.register(new Customer("6"), frenchCuisine);
        cuisinesRegistry.register(new Customer("7"), frenchCuisine);
        cuisinesRegistry.register(new Customer("8"), spanishCuisine);
        cuisinesRegistry.register(new Customer("9"), spanishCuisine);

        List<Cuisine> expected = Arrays.asList(frenchCuisine, spanishCuisine);
        assertEquals(expected, cuisinesRegistry.topCuisines(2));

    }

    @Test
    public void shouldReturnTopCuisinesWithMoreRequested() {
        cuisinesRegistry.register(new Customer("4"), italianCuisine);

        List<Cuisine> expected = Arrays.asList(italianCuisine);
        assertEquals(expected, cuisinesRegistry.topCuisines(1000));
    }

    @Test
    public void shouldReturnTopCuisinesWhenAllRequested() {
        int count = 10;

        List<Cuisine> expected = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Cuisine c = new Cuisine("cuisine" + String.valueOf(i));
            cuisinesRegistry.register(new Customer(String.valueOf(i)), c);
            expected.add(c);
        }

        assertTrue(listsMatch(cuisinesRegistry.topCuisines(count), expected));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailInvalidCountTopCuisinesRequested() {
        cuisinesRegistry.topCuisines(-1);
    }

    @Test
    public void shouldNotCountSameCustomerTwice() {
        cuisinesRegistry.register(alice, russianCuisine);
        cuisinesRegistry.register(alice, italianCuisine);
        cuisinesRegistry.register(alice, new Cuisine("russian"));

        cuisinesRegistry.register(bob, italianCuisine);
        cuisinesRegistry.register(peter, italianCuisine);

        List<Cuisine> expected = Arrays.asList(italianCuisine);
        assertEquals(expected, cuisinesRegistry.topCuisines(1));

        assertTrue(listsMatch(
                cuisinesRegistry.cuisineCustomers(russianCuisine),
                Arrays.asList(alice)
        ));

        assertTrue(listsMatch(
                cuisinesRegistry.customerCuisines(alice),
                Arrays.asList(russianCuisine, italianCuisine)
        ));
    }
}