package de.quandoo.recruitment.registry.model;

public class Cuisine implements Comparable<Cuisine> {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Cuisine)) {
            return false;
        }

        return name.equals(((Cuisine) obj).name);
    }

    @Override
    public int compareTo(Cuisine o) {
        return name.compareTo(o.name);
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + (name == null ? 0 : name.hashCode());
        return result;
    }
}
