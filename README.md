# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## Solution
Specific implementation depends on further requirements, e.g. what is the frequency ratio between the following
methods calls: register, topCuisines, customerCuisines, cuisineCustomers; which systems are going to use it?
Is consistency required?

I implemented two solutions:

1) InMemoryCuisinesRegistryFastTop: solution is rather balanced, which provides consistency and has relatively fast speeds of all operations,
                                   but depending on the stakeholders' requirements of the service, it could be changed to more optimal for specific use-case.
                                   E.g we could make frequently used methods faster at the expense of slowing down of non-frequently used methods. And/or we 
                                   could cache some expensive calls.
                                   In this solution addition of entry takes O(log N) time, where N - count of cuisines (because of topCuisine maintenance),
                                   topCuisines takes O(N) only and is always up-to-date, customerCuisines and cuisineCustomers is constant + O(K) which is
                                   the returning size of elements. We do also lock topCuisines for writing which slows down registration.

2) InMemoryCuisinesRegistryFastRegister: we do not maintain topCuisines TreeSet but calculate topCuisines on demand, once called, thus
                                         we not only increase algorithmic performance of register method to O(N), but also get rid of synchronized
                                         registerCuisine method and use ConcurrentHashMap for cuisines the same way as for customers, which will make
                                         it faster. Not locks.
                                         Drawback for this solution is that topCuisines method will be O(N*log N), so if it does not need to be called
                                         frequently or non-fresh data (from cache) is OK for stakeholders - it might be a better solution.

Steps for galactic scale:
1. Depending on requirements we could take one of the approaches as basis: but anyway it will evolve into some hybrid variants (we should either maintain sorted array - resorted on new event or sorting huge data on some external storage probably with kind of merge sorting arlgorithms)
2. Current solutions are thread-safe. They should be scalable vertically by adding more threads (we might need to increase heap size for Java).
3. We could add caching were/if possible (e.g. topCuisines can be cached and invalidated when cuisine added) or some precalculations.
4. Vertical scaling:

   Add more instances of the app running servers, hide them behind load balancer, move in-memory data to some
distributed cache system like Redis with locks support (for safe modification)

   If consistency is not the requirement and eventual consistency is fine, we could make the following, system consists of
the following modules:

    * Master API module (or pool if needed): for interaction with stakeholder, REST/Library/could be also async messaging protocol), receives a register request from stakeholder via API and puts to async queue for processing by Registrar

    * Queue broker

    * Registrar module (or pool if needed): for processing incoming register events from the queue. Registrar reads data from queue, once received - persists in a row of DS safely locking access to this row which is being indexed (partitioned data if needed)

    * In-memory distributed data store (DS) cluster with lock capabilities like Redis

    * Analyzer for running periodical (or even based - e.g. for top cuisines - new X cuisines registered with more than Y customers) like calculating top cuisines and storing them in DS

    * Balancer (in case of synchronous interaction between client and API) for balancing load between Masters

Notes:
- When running sort query for topCuisines calculation there might be consistency problem or DS might be busy with 
calculation which means it won't accept registrar tasks until it finished with query, to solve it we could use read-only 
replica and run analyzer queries on slave
- To make run_sort_query run faster we could also filter out most probably very significant amount of records by excluding 
those cuisines which have less than X (small number - to be discusses with business) customers, because it does 
not seem significant on big numbers for this use-case

```
CLIENT -- register --> BALANCER --> MASTER(S) -- register --> [][][][] QUEUE --> REGISTRAR(S) -- write --> DS

CLIENT -- customerCuisines(cuisineCustomers) --> BALANCER --> MASTER(S) --> -- read --> DS

CLIENT -- topCuisines --> BALANCER --> MASTER(S) -- get_top_cuisines --> DS <-- run_sort_query & store_top_cuisines --  ANALYZER <-- scheduler/event
```
Along with clarification of requirements for the system the modules and even the whole system might change, so I'd opt for discussing this topic in more details.
 