package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;

public class InMemoryCuisinesRegistryFastRegister implements CuisinesRegistry {
    private ConcurrentHashMap<Customer, Set<Cuisine>> customerCuisines = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Cuisine, Set<Customer>> cuisineCustomers = new ConcurrentHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        requireNonNull(customer, "Customer can not be null");
        requireNonNull(cuisine, "Cuisine can not be null");

        customerCuisines.putIfAbsent(customer, Collections.synchronizedSet(new HashSet<>()));
        customerCuisines.get(customer).add(cuisine);

        cuisineCustomers.putIfAbsent(cuisine, Collections.synchronizedSet(new HashSet<>()));
        cuisineCustomers.get(cuisine).add(customer);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        requireNonNull(cuisine, "Cuisine can not be null");
        return new ArrayList<>(cuisineCustomers.getOrDefault(cuisine, new HashSet<>()));
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        requireNonNull(customer, "Customer can not be null");
        return new ArrayList<>(customerCuisines.getOrDefault(customer, new HashSet<>()));
    }

    @Override
    public List<Cuisine> topCuisines(final int n) throws IllegalArgumentException {
        if (n < 0) {
            throw new IllegalArgumentException("Invalid argument " + n);
        }
        // TODO: add caching here if full consistency is not required
        HashMap<Cuisine, Integer> cuisinesRankings = new HashMap<>();
        for (Cuisine c: cuisineCustomers.keySet()) {
            cuisinesRankings.put(c, cuisineCustomers.get(c).size());
        }
        List<Map.Entry<Cuisine, Integer>> list = new ArrayList<>(cuisinesRankings.entrySet());
        list.sort(Comparator.comparing(
                (Map.Entry<Cuisine, Integer> entry) -> cuisinesRankings.get(entry.getKey()))
                .reversed()
                .thenComparing(Map.Entry::getKey));

        ArrayList<Cuisine> res = new ArrayList<>(n);

        Iterator it = list.iterator();
        for (int i = 0; i < n; i++) {
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                res.add((Cuisine) entry.getKey());
            } else {
                break;
            }
        }

        return res;
    }
}
